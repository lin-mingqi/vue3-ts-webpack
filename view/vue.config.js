const Components = require('unplugin-vue-components/webpack')

const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')
module.exports = {
  //   ...
  configureWebpack: {
    plugins: [
      Components({
        resolvers: [ElementPlusResolver()],
      }),
      // new webpack.NormalModuleReplacementPlugin(
      //   /element-plus[\/\\]lib[\/\\]locale[\/\\]lang[\/\\]en/,
      //   'element-plus/lib/locale/lang/zh-cn'
      // ),
    ],
    module: {
      rules: [
        {
          test: /\.mjs$/,
          include: /node_modules/,
          type: 'javascript/auto',
        },
      ],
    }
  }
}
