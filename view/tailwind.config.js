module.exports = {
  purge: [],
  mode: 'jit',
  purge: ['./src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
    },
    textColor: {
      'white': '#fff',
    }
  },
  variants: {
    extend: {
      opacity: ['active'],
      backgroundColor: ['active'],
      boxShadow: ['active'],
    },
  },
  plugins: [],
}
