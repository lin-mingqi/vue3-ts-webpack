import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './api'
import './assets/css/tailwindcss.css'
import 'animate.css' // npm install animate.css --save安装，在引入

const app = createApp(App)
app.use(router)
app.mount('#app')
