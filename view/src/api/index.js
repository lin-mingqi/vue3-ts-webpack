import request from "./request";
const requireApi = require.context("./modules", false, /\.js$/);
const api = requireApi.keys().reduce((result, path) => {
  // 截取./sss.中建的sss
  const moduleName = path.replace(/^\.\/([\w-]+)\.js$/, "$1");
  const moduleContent = requireApi(path).default || [];
  const moduleApi = moduleContent.reduce((pre, cur) => {
    return {
      ...pre,
      [cur.name]: function (params = {}, contentType) {
        const method = request[cur.method];
        return method(
          cur.url,
          {
            // 传参，一般默认不带cur参数
            ...cur.params,
            ...params
          },
          contentType
        );
      }
    };
  }, {});
  return {
    ...result,
    [moduleName]: moduleApi
  };
}, {});
export default api;
