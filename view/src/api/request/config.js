import axios from "axios";

const http = axios.create({
  baseURL: '',
  timeout: 5000
});

// 请求拦截处理
// 新增token处理
http.interceptors.request.use((res) => {
  // token处理
  // 检查data参数是否存在token字段并且为headers添加token
  if (res.data?.token) {
    res.headers["token"] = res.data.token;
  }
  return res;
});

http.interceptors.response.use(
  ({ status, data, config: { url } }) => {
    if (status === 200) {
      if (data.code === 200 || data.status === 200) {
        return data.data;
      } else {
        return Promise.reject(data);
      }
    } else {
      return Promise.reject(data);
    }
  },
  (error) => {
    return Promise.reject(error.response || error);
  }
);

export default http;
