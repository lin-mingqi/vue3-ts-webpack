import http from './config';

export const httpGet = (url, data, contentType = 'application/json') => {
  return http({
    url,
    method: 'get',
    data,
    headers: { 'content-type': contentType },
  });
};

export const httpPost = (url, data, contentType = 'application/json') => {
  return http({
    url,
    method: 'post',
    data,
    headers: { 'content-type': contentType },
  });
};

export const httpPut = (url, data,contentType = 'application/json') => {
  return http({
    url,
    method: 'put',
    data,
    headers: { 'content-type': contentType },
  });
};

export const httpDelete = (url, data, contentType = 'application/json') => {
  return http({
    url: id ? `${url}/${id}` : url,
    method: 'detele',
    data,
    headers: { 'content-type': contentType },
  });
};


export default {
  get: httpGet,
  post: httpPost,
  put: httpPut,
  delete: httpDelete,
};
