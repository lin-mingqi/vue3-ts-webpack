/***
 * 判断当前运行环境
 * @returns [string]可能值 ios / android / pc / other
 * */
export const os = (function () {
    var ua = navigator.userAgent.toLowerCase();
    return /(?:iphone|ipad|ipod)/.test(ua)
      ? "ios"
      : /(?:android|adr )/.test(ua)
      ? "android"
      : !/mobile/.test(ua)
      ? "pc"
      : "other";
  })();
