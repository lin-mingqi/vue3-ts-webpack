import { RouteRecordRaw } from 'vue-router'
const Home = () => import('@/views/pc/home.vue' /* webpackChunkName: "home" */)
const Record = () => import('@/views/pc/record.vue' /* webpackChunkName: "redcord" */)
const Class = () => import('@/views/pc/class.vue' /* webpackChunkName: "class" */)
const Demo = () => import('@/views/pc/demo.vue' /* webpackChunkName: "demo" */)
const Line = () => import('@/views/pc/line.vue' /* webpackChunkName: "line" */)
const Article = () => import('@/views/pc/article.vue' /* webpackChunkName: "article" */)
const Comic = () => import('@/views/pc/comic.vue' /* webpackChunkName: "comic" */)
const Message = () => import('@/views/pc/message.vue' /* webpackChunkName: "message" */)
const Board = () => import('@/views/pc/board.vue' /* webpackChunkName: "board" */)
// const Photo = () => import('@/views/pc/photo.vue' /* webpackChunkName: "photo" */)

const pcRoutes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/board',
    name: 'board',
    component: Board,
  },
  {
    path: '/message',
    name: 'message',
    component: Message,
  },
  {
    path: '/article',
    name: 'article ',
    component: Article,
  },
  {
    path: '/class',
    name: 'class',
    component: Class,
  },
  {
    path: '/record',
    name: 'record',
    component: Record,
  },
  {
    path: '/line',
    name: 'line',
    component: Line,
  },
  {
    path: '/demo',
    name: 'demo',
    component: Demo,
  },
  {
    path: '/comic',
    name: 'comic',
    component: Comic,
  },
]

export default pcRoutes
