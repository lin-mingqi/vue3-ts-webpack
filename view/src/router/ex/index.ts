import { RouteRecordRaw } from 'vue-router'
const Home = () => import('@/views/ex/home.vue' /* webpackChunkName: "home" */);
const Login = () => import('@/views/ex/login.vue' /* webpackChunkName: "login" */);
const exRoutes: Array<RouteRecordRaw> = [
  {
    path: '/ex-home',
    name: 'exHome',
    component: Home,
  },
  {
    path: '/login',
    name: 'exHome',
    component: Login,
  }
];
export default exRoutes;