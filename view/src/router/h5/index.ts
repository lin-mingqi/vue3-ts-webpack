import { RouteRecordRaw } from 'vue-router'
const Home = () => import('@/views/h5/home.vue' /* webpackChunkName: "home" */);
const h5Routes: Array<RouteRecordRaw> = [
  {
    path: '/h5-home',
    name: 'h5Home',
    component: Home,
  }
];
export default h5Routes;