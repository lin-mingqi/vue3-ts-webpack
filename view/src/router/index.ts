import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import pcRoutes from './pc'
import h5Routes from './h5'
import exRoutes from './ex'
// import safeRoutes from './safe'
const routes: Array<RouteRecordRaw> = [
  ...pcRoutes, 
  ...h5Routes, 
  ...exRoutes,
  
  // ...safeRoutes
]
const router = createRouter({
  history: createWebHistory(),
  routes
})

// 给路由设置守卫函数
router.beforeEach((to, from, next) => {
  document.body.scrollTop = 0
  document.documentElement.scrollTop = 0
  next()
})


export default router
