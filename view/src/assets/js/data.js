const echarts = require('echarts/lib/echarts')
export const pie = {
  title: {
    text: '',
    left: 'center',
  },
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)',
  },
  legend: {
    left: 'center',
    bottom: '3%',
  },
  toolbox: {
    show: true,
    feature: {
      mark: { show: true },
      dataView: { show: true, readOnly: false },
      restore: { show: true },
      saveAsImage: { show: true },
    },
  },
  series: [
    {
      name: '技能树',
      type: 'pie',
      radius: [20, 100],
      center: ['50%', '50%'],
      roseType: 'area',
      itemStyle: {
        borderRadius: 5,
      },
      data: [
        { value: 30, name: 'CSS' },
        { value: 22, name: 'JavaScript' },
        { value: 20, name: 'Vue2.x' },
        { value: 24, name: 'npm' },
        { value: 15, name: 'webpack' },
        { value: 20, name: 'vite' },
        { value: 18, name: 'Vue3.x' },
        { value: 26, name: 'H5' },
        { value: 10, name: '百度地图api' },
        { value: 20, name: '第三方库' },
      ],
    },
  ],
}
export const time = {
  series: [
    {
      name: 'hour',
      type: 'gauge',
      startAngle: 90,
      endAngle: -270,
      min: 0,
      max: 12,
      splitNumber: 12,
      clockwise: true,
      axisLine: {
        lineStyle: {
          width: 15,
          color: [[1, 'rgba(0,0,0,0.7)']],
          shadowColor: 'rgba(0, 0, 0, 0.5)',
          shadowBlur: 15,
        },
      },
      splitLine: {
        lineStyle: {
          shadowColor: 'rgba(0, 0, 0, 0.3)',
          shadowBlur: 3,
          shadowOffsetX: 1,
          shadowOffsetY: 2,
        },
      },
      axisLabel: {
        fontSize: 20,
        distance: 25,
        formatter: function(value) {
          if (value === 0) {
            return ''
          }
          return value + ''
        },
      },
      anchor: {
        show: true,
        icon:
          'path://M532.8,70.8C532.8,70.8,532.8,70.8,532.8,70.8L532.8,70.8C532.7,70.8,532.8,70.8,532.8,70.8z M456.1,49.6c-2.2-6.2-8.1-10.6-15-10.6h-37.5v10.6h37.5l0,0c2.9,0,5.3,2.4,5.3,5.3c0,2.9-2.4,5.3-5.3,5.3v0h-22.5c-1.5,0.1-3,0.4-4.3,0.9c-4.5,1.6-8.1,5.2-9.7,9.8c-0.6,1.7-0.9,3.4-0.9,5.3v16h10.6v-16l0,0l0,0c0-2.7,2.1-5,4.7-5.3h10.3l10.4,21.2h11.8l-10.4-21.2h0c6.9,0,12.8-4.4,15-10.6c0.6-1.7,0.9-3.5,0.9-5.3C457,53,456.7,51.2,456.1,49.6z M388.9,92.1h11.3L381,39h-3.6h-11.3L346.8,92v0h11.3l3.9-10.7h7.3h7.7l3.9-10.6h-7.7h-7.3l7.7-21.2v0L388.9,92.1z M301,38.9h-10.6v53.1H301V70.8h28.4l3.7-10.6H301V38.9zM333.2,38.9v10.6v10.7v31.9h10.6V38.9H333.2z M249.5,81.4L249.5,81.4L249.5,81.4c-2.9,0-5.3-2.4-5.3-5.3h0V54.9h0l0,0c0-2.9,2.4-5.3,5.3-5.3l0,0l0,0h33.6l3.9-10.6h-37.5c-1.9,0-3.6,0.3-5.3,0.9c-4.5,1.6-8.1,5.2-9.7,9.7c-0.6,1.7-0.9,3.5-0.9,5.3l0,0v21.3c0,1.9,0.3,3.6,0.9,5.3c1.6,4.5,5.2,8.1,9.7,9.7c1.7,0.6,3.5,0.9,5.3,0.9h33.6l3.9-10.6H249.5z M176.8,38.9v10.6h49.6l3.9-10.6H176.8z M192.7,81.4L192.7,81.4L192.7,81.4c-2.9,0-5.3-2.4-5.3-5.3l0,0v-5.3h38.9l3.9-10.6h-53.4v10.6v5.3l0,0c0,1.9,0.3,3.6,0.9,5.3c1.6,4.5,5.2,8.1,9.7,9.7c1.7,0.6,3.4,0.9,5.3,0.9h23.4h10.2l3.9-10.6l0,0H192.7z M460.1,38.9v10.6h21.4v42.5h10.6V49.6h17.5l3.8-10.6H460.1z M541.6,68.2c-0.2,0.1-0.4,0.3-0.7,0.4C541.1,68.4,541.4,68.3,541.6,68.2L541.6,68.2z M554.3,60.2h-21.6v0l0,0c-2.9,0-5.3-2.4-5.3-5.3c0-2.9,2.4-5.3,5.3-5.3l0,0l0,0h33.6l3.8-10.6h-37.5l0,0c-6.9,0-12.8,4.4-15,10.6c-0.6,1.7-0.9,3.5-0.9,5.3c0,1.9,0.3,3.7,0.9,5.3c2.2,6.2,8.1,10.6,15,10.6h21.6l0,0c2.9,0,5.3,2.4,5.3,5.3c0,2.9-2.4,5.3-5.3,5.3l0,0h-37.5v10.6h37.5c6.9,0,12.8-4.4,15-10.6c0.6-1.7,0.9-3.5,0.9-5.3c0-1.9-0.3-3.7-0.9-5.3C567.2,64.6,561.3,60.2,554.3,60.2z',
        showAbove: false,
        offsetCenter: [0, '-35%'],
        size: 0,
        keepAspect: true,
        itemStyle: {
          color: '#707177',
        },
      },
      pointer: {
        icon:
          'path://M2.9,0.7L2.9,0.7c1.4,0,2.6,1.2,2.6,2.6v115c0,1.4-1.2,2.6-2.6,2.6l0,0c-1.4,0-2.6-1.2-2.6-2.6V3.3C0.3,1.9,1.4,0.7,2.9,0.7z',
        width: 12,
        length: '55%',
        offsetCenter: [0, '8%'],
        itemStyle: {
          color: '#C0911F',
          shadowColor: 'rgba(0, 0, 0, 0.3)',
          shadowBlur: 8,
          shadowOffsetX: 2,
          shadowOffsetY: 4,
        },
      },
      detail: {
        show: false,
      },
      title: {
        offsetCenter: [0, '30%'],
      },
      data: [
        {
          value: 0,
        },
      ],
    },
    {
      name: 'minute',
      type: 'gauge',
      startAngle: 90,
      endAngle: -270,
      min: 0,
      max: 60,
      clockwise: true,
      axisLine: {
        show: false,
      },
      splitLine: {
        show: false,
      },
      axisTick: {
        show: false,
      },
      axisLabel: {
        show: false,
      },
      pointer: {
        icon:
          'path://M2.9,0.7L2.9,0.7c1.4,0,2.6,1.2,2.6,2.6v115c0,1.4-1.2,2.6-2.6,2.6l0,0c-1.4,0-2.6-1.2-2.6-2.6V3.3C0.3,1.9,1.4,0.7,2.9,0.7z',
        width: 8,
        length: '70%',
        offsetCenter: [0, '8%'],
        itemStyle: {
          color: '#C0911F',
          shadowColor: 'rgba(0, 0, 0, 0.3)',
          shadowBlur: 8,
          shadowOffsetX: 2,
          shadowOffsetY: 4,
        },
      },
      anchor: {
        show: true,
        size: 20,
        showAbove: false,
        itemStyle: {
          borderWidth: 15,
          borderColor: '#C0911F',
          shadowColor: 'rgba(0, 0, 0, 0.3)',
          shadowBlur: 8,
          shadowOffsetX: 2,
          shadowOffsetY: 4,
        },
      },
      detail: {
        show: false,
      },
      title: {
        offsetCenter: ['0%', '-40%'],
      },
      data: [
        {
          value: 0,
        },
      ],
    },
    {
      name: 'second',
      type: 'gauge',
      startAngle: 90,
      endAngle: -270,
      min: 0,
      max: 60,
      animationEasingUpdate: 'bounceOut',
      clockwise: true,
      axisLine: {
        show: false,
      },
      splitLine: {
        show: false,
      },
      axisTick: {
        show: false,
      },
      axisLabel: {
        show: false,
      },
      pointer: {
        icon:
          'path://M2.9,0.7L2.9,0.7c1.4,0,2.6,1.2,2.6,2.6v115c0,1.4-1.2,2.6-2.6,2.6l0,0c-1.4,0-2.6-1.2-2.6-2.6V3.3C0.3,1.9,1.4,0.7,2.9,0.7z',
        width: 4,
        length: '85%',
        offsetCenter: [0, '8%'],
        itemStyle: {
          color: '#C0911F',
          shadowColor: 'rgba(0, 0, 0, 0.3)',
          shadowBlur: 8,
          shadowOffsetX: 2,
          shadowOffsetY: 4,
        },
      },
      anchor: {
        show: true,
        size: 15,
        showAbove: true,
        itemStyle: {
          color: '#C0911F',
          shadowColor: 'rgba(0, 0, 0, 0.3)',
          shadowBlur: 8,
          shadowOffsetX: 2,
          shadowOffsetY: 4,
        },
      },
      detail: {
        show: false,
      },
      title: {
        offsetCenter: ['0%', '-40%'],
      },
      data: [
        {
          value: 0,
        },
      ],
    },
  ],
}
export const bar = {
  title: {},
  tooltip: {},
  xAxis: {
    data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子'],
  },
  yAxis: {},
  series: [
    {
      name: '销量',
      type: 'effectScatter',
      data: [5, 20, 36, 10, 10, 20],
    },
  ],
}
export const line = {
  //设置线条的颜色，后面是个数组
  color: ['pink', 'red', 'green', 'blue', 'gray'],
  //设置图表标题
  title: {
    text: '',
  },
  //图表的提示框组件
  tooltip: {
    //触发方式 - 坐标轴
    trigger: 'axis',
  },
  //图例组件
  legend: {
    //series有name了，这里的data可以删除掉
    data: ['心情', '学习', '工作', '娱乐', '消费'],
  },
  //网格配置 grid可以控制线形图 柱状图 图标大小
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    //是否显示刻度标签
    containLabel: true,
  },
  //工具箱组件，可以另存为图片等功能
  toolbox: {
    feature: {
      saveAsImage: {},
    },
  },
  //设置x轴的相关配置
  xAxis: {
    type: 'category',
    //线条和y轴是否有缝隙
    boundaryGap: false,
    data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
  },
  //设置y轴的相关配置
  yAxis: {
    type: 'value',
  },
  //系列图表配置，决定显示那种类型的图表
  series: [
    {
      name: '学习',
      type: 'line',
      //总量，后面的会堆叠前面的累加起来，删除掉就会折叠了，一般不需要
      //stack: '总量',
      data: [250, 190, 300, 270, 500, 300, 0],
    },
    {
      name: '娱乐',
      type: 'line',
      //stack: '总量',
      data: [20, 30, 25, 20, 10, 330, 300],
    },
    {
      name: '心情',
      type: 'line',
      //stack: '总量',
      data: [150, 232, 201, 154, 190, 340, 410],
    },
    {
      name: '消费',
      type: 'line',
      //stack: '总量',
      data: [320, 332, 301, 334, 390, 300, 320],
    },
    {
      name: '工作',
      type: 'line',
      //stack: '总量',
      data: [520, 932, 901, 934, 1290, 500, 0],
    },
  ],
}
export const cityMap = {
  series: [
    {
      name: 'map',
      type: 'map', // 地图
      map: 'China', // 加载注册的地图
      selectedMode: false, // 不让单独选中
      roam: true, // 开始鼠标事件，scale缩放、move移动
      // 图形上的文本标签
      label: {
        show: true,
        color: '#000a3c',
      },
      // 地图样式
      itemStyle: {
        // 区域样式
        areaColor: {
          type: 'radial',
          x: 0.5,
          y: 0.5,
          r: 3,
          colorStops: [
            {
              offset: 0,
              color: 'rgba(223, 231, 242, 1)', // 0% 处的颜色
            },
            {
              offset: 1,
              color: 'rgba(2, 99, 206, 1)', // 100% 处的颜色
            },
          ],
          globalCoord: false, // 缺省为 false
        },
        borderWidth: 1, // 边框大小
        borderColor: 'rgba(104, 152, 190, 1)', // 边框样式
        shadowColor: 'rgba(128, 217, 248, 1)', // 阴影颜色
        shadowOffsetX: -2, // 阴影水平方向上的偏移距离
        shadowOffsetY: 2, // 阴影垂直方向上的偏移距离
        shadowBlur: 10, // 文字块的背景阴影长度
      },
      // 选中状态下样式
      emphasis: {
        label: {
          color: '#ffffff',
        },
        itemStyle: {
          areaColor: '#a5d4fe',
        },
      },
    },
  ],
}

export const radar = {
  color: ['#67F9D8', '#FFE434', '#56A3F1', '#FF917C'],
  title: {},
  legend: {},
  radar: [
    {
      indicator: [
        { text: 'Indicator1' },
        { text: 'Indicator2' },
        { text: 'Indicator3' },
        { text: 'Indicator4' },
        { text: 'Indicator5' },
      ],
      center: ['25%', '50%'],
      radius: 90,
      startAngle: 90,
      splitNumber: 4,
      shape: 'circle',
      axisName: {
        formatter: '【{value}】',
        color: '#428BD4',
      },
      splitArea: {
        areaStyle: {
          color: ['#77EADF', '#26C3BE', '#64AFE9', '#428BD4'],
          shadowColor: 'rgba(0, 0, 0, 0.2)',
          shadowBlur: 10,
        },
      },
      axisLine: {
        lineStyle: {
          color: 'rgba(211, 253, 250, 0.8)',
        },
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(211, 253, 250, 0.8)',
        },
      },
    },
    {
      indicator: [
        { text: 'Indicator1', max: 150 },
        { text: 'Indicator2', max: 150 },
        { text: 'Indicator3', max: 150 },
        { text: 'Indicator4', max: 120 },
        { text: 'Indicator5', max: 108 },
        { text: 'Indicator6', max: 72 },
      ],
      center: ['75%', '50%'],
      radius: 90,
      axisName: {
        color: '#fff',
        backgroundColor: '#666',
        borderRadius: 3,
        padding: [3, 5],
      },
    },
  ],
  series: [
    {
      type: 'radar',
      emphasis: {
        lineStyle: {
          width: 4,
        },
      },
      data: [
        {
          value: [100, 8, 0.4, -80, 2000],
          name: 'Data A',
        },
        {
          value: [60, 5, 0.3, -100, 1500],
          name: 'Data B',
          areaStyle: {
            color: 'rgba(255, 228, 52, 0.6)',
          },
        },
      ],
    },
    {
      type: 'radar',
      radarIndex: 1,
      data: [
        {
          value: [120, 118, 130, 100, 99, 70],
          name: 'Data C',
          symbol: 'rect',
          symbolSize: 12,
          lineStyle: {
            type: 'dashed',
          },
          label: {
            show: true,
            formatter: function(params) {
              return params.value
            },
          },
        },
        {
          value: [100, 93, 50, 90, 70, 60],
          name: 'Data D',
          areaStyle: {
            color: new echarts.graphic.RadialGradient(0.1, 0.6, 1, [
              {
                color: 'rgba(255, 145, 124, 0.1)',
                offset: 0,
              },
              {
                color: 'rgba(255, 145, 124, 0.9)',
                offset: 1,
              },
            ]),
          },
        },
      ],
    },
  ],
}


const cellSize = [60, 60]
function getVirtulData() {
  let date = +echarts.number.parseDate('2017-02-01')
  let end = +echarts.number.parseDate('2017-03-01')
  let dayTime = 3600 * 24 * 1000
  let data = []
  for (let time = date; time < end; time += dayTime) {
    data.push([
      echarts.format.formatTime('yyyy-MM-dd', time),
      Math.floor(Math.random() * 10000),
    ])
  }
  return data
}
const scatterData = getVirtulData()
export const calendar = {
  tooltip: {},
  legend: {
    data: ['Work', 'Entertainment', 'Sleep'],
    bottom: 20,
  },
  calendar: {
    top: 'middle',
    left: 'center',
    orient: 'vertical',
    cellSize: cellSize,
    yearLabel: {
      show: false,
      fontSize: 30,
    },
    dayLabel: {
      margin: 20,
      firstDay: 1,
      nameMap: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'],
    },
    monthLabel: {
      show: false,
    },
    range: ['2017-02'],
  },
  series: [
    {
      id: 'label',
      type: 'scatter',
      coordinateSystem: 'calendar',
      symbolSize: 1,
      label: {
        show: true,
        formatter: function(params) {
          return echarts.format.formatTime('dd', params.value[0])
        },
        offset: [-cellSize[0] / 2 + 10, -cellSize[1] / 2 + 10],
        fontSize: 10,
      },
      data: scatterData,
    },
  ],
}
