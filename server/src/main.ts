import express from 'express'
const server = express()

// 配置解析表单请求体 application/json
server.use(express.json())
// 配置解析表单请求体 application/x-www-form-urlencoded
server.use(express.urlencoded({ extended: true }))

server.use('/api', require('./route/route'))

server.listen(5408, () => console.log('服务已启动\nhttp://localhost:5408'))