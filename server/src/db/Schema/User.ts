import mongo from '../connect'

const UserSchema = new mongo.Schema({
  name: {
    require: true,
    type: String,
    minlength: [2, 'name 至少有 2 位'],
    maxlength: [6, 'name 至多有 6 位']
  }
})

export default UserSchema