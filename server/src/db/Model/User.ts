import mongo from '../connect'
import UserSchema from '../Schema/User'

const UserModel = mongo.model('Users', UserSchema)

export default UserModel