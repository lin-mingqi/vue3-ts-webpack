import { Router, Request, Response } from 'express'
const article = Router()

article.get('/', (req: Request, res: Response) => {
  res.send('article')
})

module.exports = article
