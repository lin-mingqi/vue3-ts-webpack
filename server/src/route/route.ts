import { Router } from 'express'
const route = Router()

route.use('/user', require('./user/user'))
route.use('/article', require('./article/article'))

module.exports = route
