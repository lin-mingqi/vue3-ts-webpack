import { Router, Request, Response } from 'express'
import UserModel from '../../db/Model/User'
const user = Router()

user.get('/', (req: Request, res: Response) => {
  UserModel.find({})
    .then((result: Object) => res.send(result))
    .catch((err: Object) => res.send(err))
})

user.put('/', (req: Request, res: Response) => {
  new UserModel(req.body)
    .save()
    .then((result: Object) => res.send(result))
    .catch((err: Error) => res.send(err.message))
})

module.exports = user
